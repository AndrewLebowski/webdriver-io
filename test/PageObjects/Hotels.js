const Page = require('./Page');
const Helpers = require('../Helpers');


class Hotels extends Page {

    //buttons
    get closeCalendarButton() {
        return $('.c2-calendar-close-button');
    }


    //values
    get adultsAmountValue() {
        return $('#group_adults option[selected]').getAttribute('value');
    }
    get childrenAmountValue() {
        return $('#group_children option[selected]').getAttribute('value');
    }
    get roomsAmountValue() {
        return $('#no_rooms option[selected]').getAttribute('value');
    }
    get cityValue() {
        return $('#ss').getValue();
    }
   

    
    //arrays
    get childrenSelectedInputsArray() {
        return $$('.sb-group__children__field option[selected]');
    }
    get checkboxesArray() {
        return $$('[data-id="filter_class"] a');
    }
    get hotelsArray() {
        return $$('#hotellist_inner .sr_item');
    }
    get datesArray() {
        return $$('[data-offset="262"] td.c2-day');
    }
    get seeAvailabilityButtonsArray() {
        return $$('.b-button__text');
    }
    get reviewScoresArray() {
        return $$('[data-name="review_score"]');
    }
    


    //helpers
    get firstDateSeconds() {
        return $('.c2-day-s-first-in-range').getAttribute('data-id'); 
    }
    get lastDateSeconds() {
        return $('.c2-day-s-last-in-range').getAttribute('data-id'); 
    }
    get calendar() {
        return $('.sb-date-field__wrapper');
    }
    get hotelsContainer() {
        return $('#ajaxsrwrap');
    }
    get calendarWindow() {
        return $('.c2-wrapper .c2-calendar');
    }

   

    //methods
    submit() {
        $('.sb-searchbox__button').click();
    };
 
    getChildAges() {
        return this.childrenSelectedInputsArray.map( item => {
            return item.getValue();          
        })     
    };
   
    checkIfCalendarOpened() {
        const elementClass = this.calendarWindow.getAttribute('class');
        return !elementClass.includes('c2-wrapper-s-hidden');
    };

    checkHotelStatus() {
        const results = [];
        const array = this.hotelsArray;
        for( let i = 0; i < 5; i++ ) {
            const result = array[i].$('.roomrow').isDisplayed() ||                  // it checks only first 5 hotels because Webdriver IO has problems                                           
                           array[i].$('.sr_item_content_row').isDisplayed();       // with iteration of arrays with 5 elements and more in Mozilla
            results.push(result);
        }
        return !results.includes(false);
        
    };

    chooseRandomDate() {   
        const randomDate = Helpers.getRandomInt(1, 27);
        const arrayOfAvailableDates = this.datesArray.filter( item => {
            return item.isDisplayed();
        })     
        arrayOfAvailableDates[randomDate].click();
    };

    clickRandomDetailsButton() {
        const buttons = this.seeAvailabilityButtonsArray;
        const randomInt = Helpers.getRandomInt(0, buttons.length);
        buttons[randomInt].click();
    };
    
};

module.exports = new Hotels();