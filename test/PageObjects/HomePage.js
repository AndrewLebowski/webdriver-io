const Page = require('./Page');
const Helpers = require('../Helpers');

class HomePage extends Page {


    //buttons
    get selectPeopleInputButton() { 
        return $('#xp__guests__toggle'); 
    }   
    get addAdultButton() {
        return $('.sb-group__field-adults .bui-stepper__add-button'); 
    }
    get addChildButton() {
        return $('.sb-group-children .bui-stepper__add-button'); 
    }
    get subtractChildButton() {
        return $('.sb-group-children .bui-stepper__subtract-button');
    }
    get addRoomButton() {
        return $('.sb-group__field-rooms .bui-stepper__add-button'); 
    }
    get addMonthButton() {
        return $('.bui-calendar__control--next');
    }
    get submitButton() {
        return $('[type="submit"]');
    }


    //values
    get adultsAmountValue() {
        return $('#group_adults').getValue();
    }
    get childrenAmountValue() {
        return $('#group_children').getValue();
    }
    get roomsAmountValue() {
        return $('#no_rooms').getValue();
    }
    get cityValue() {
        return $('#ss').getValue();
    };


    //arrays
    get childrenInputsArray() {
        return $$('.sb-group__children__field select');
    }
    get childrenSelectedInputsArray() {
        return $$('.sb-group__children__field option[selected]');
    }
    get datesArray() {
        return $$('.bui-calendar__content td[data-date]');
    }         
    get selectedDatesArray() {
        return $$('.bui-calendar__date--selected');
    }
    get inputCityListItemsArray() {
        return $$('ul[role="listbox"] li');
    }
    get cityList() {   
        const largeCityItems = $$('.promotion-postcard__large');
        const smallCityItems = $$('.promotion-postcard__small');
        return largeCityItems.concat(smallCityItems);
    }
    


    //helpers
    get inputCityList() {
        return $('ul[role="listbox"]');
    }
    get onMapCheckpoint() {
        return $('[for="sb_results_on_map"]');
    }
 

    //methods
    open() {
        super.open('https://www.booking.com/');
    };

    submit() {
        this.submitButton.click();
    };

    chooseEnglishLanguage() {
        $('.uc_language ').click();
        $('.lang_en-us').click();
    };

    checkIfAgesAreSelected() {  
        let agesAreSelected = '';
        const arr = [];
        this.childrenSelectedInputsArray.forEach( item => {          
            if(item.getAttribute('class') ==='sb_child_ages_empty_zero') {
                arr.push(false);
            } else {
                arr.push(true);
            }       
        });
  
        if(arr.includes(false)) {
            agesAreSelected = false;
        } else {
            agesAreSelected = true;
        }
        return agesAreSelected;    
    };

    clickOnElementNTimes(button, randomNum) {
        for(let i = 0; i < randomNum; i++) {
            button.click();
        } 
    };

    selectRandomPeopleAmount() {
        const addAdultsClicks = Helpers.getRandomInt(1, 10);
        const addChildrenClicks = Helpers.getRandomInt(1, 10);
        const addRoomClicks = Helpers.getRandomInt(1, 5);

        Helpers.clickOnElementNTimes( this.addChildButton, addChildrenClicks );
        Helpers.clickOnElementNTimes( this.addAdultButton, addAdultsClicks );
        Helpers.clickOnElementNTimes( this.addRoomButton, addRoomClicks  );
    };

    chooseCity(value) {
        $('#ss').setValue(value);
        this.inputCityList.waitForDisplayed();
        const randomListItem = Helpers.getRandomInt(0, this.inputCityListItemsArray.length);
        this.inputCityListItemsArray[randomListItem].click();     
    };

    chooseRandomDate() {
        //random month
        const randomMonth = Helpers.getRandomInt(1, 14);
        const addMonth = this.addMonthButton;
        Helpers.clickOnElementNTimes( addMonth, randomMonth);

        //random dates
        const randomFirstDate = Helpers.getRandomInt(1, 14);
        const randomLastDate = Helpers.getRandomInt(1, 14);

        const arrayOfAvailableDates = [];
        this.datesArray.forEach( item => {
            if( item.getAttribute('class') !== 'bui-calendar__date bui-calendar__date--disabled' ) {
                arrayOfAvailableDates.push(item);
            }
                   
        });
        arrayOfAvailableDates[randomFirstDate].click();
        arrayOfAvailableDates[randomFirstDate + randomLastDate].click();      
    };

    getMiliseconds() {
        let firstDateSeconds = '';
        let lastDateSeconds = '';      

            this.selectedDatesArray.forEach( item => {
                const currentDate = item.getAttribute('data-date');
                const currentDateSeconds = new Date(currentDate).getTime();
                if (firstDateSeconds == '') {
                    firstDateSeconds = currentDateSeconds;
                } else {
                    lastDateSeconds = currentDateSeconds;
                }
            });

        return [firstDateSeconds, lastDateSeconds];
    };

    selectChildrenAge() {
        this.childrenInputsArray.forEach( item => {
            item.click();
            const randomAge = Helpers.getRandomInt(0, 17)
            item.$(`[value="${randomAge}"]`).click();
        })
    };

    getChildAges() {
        return this.childrenSelectedInputsArray.map( item => {
            return item.getValue();          
        })     
    };

    clearChildrenValue() {
        while(this.subtractChildButton.isEnabled()) {
            this.subtractChildButton.click();
        }
        
    };

    chooseCityFromList() {
        const max = this.cityList.length;
        const randomInt = Helpers.getRandomInt(0, max);
        this.cityList[randomInt].click();
    };
};

module.exports = new HomePage();