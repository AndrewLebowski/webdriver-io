class Helpers {

    getRandomInt(min, max) {
        return Math.floor(Math.random() * (max - min) + min);
    };

    clickOnElementNTimes(button, randomNum) {
        for(let i = 0; i < randomNum; i++) {
            button.click();
        } 
    };

    checkArraysMatching(arr1, arr2) {
        let a = arr1.sort().join(', ');
        let b = arr2.sort().join(', ');
        return a === b;
    }; 


}

module.exports = new Helpers();