const HomePage = require('../PageObjects/HomePage')
const Hotels = require('../PageObjects/Hotels')
const Helpers = require('../Helpers')
const assert = require('assert')




describe('Booking.com, integrate testing', () => {

    beforeEach(function () {
        browser.deleteCookies();
    });

    it('User is able to specify age of each child', () => {

        HomePage.open();      

        //select random ages of children
        HomePage.selectPeopleInputButton.click(); 
        const randomInteger = Helpers.getRandomInt( 1, 10 );
        const addChild = HomePage.addChildButton;
        HomePage.clickOnElementNTimes( addChild, randomInteger );
        HomePage.selectChildrenAge();


        //children amount is equal to random integer
        const children = HomePage.childrenAmountValue;
        assert.equal(children, randomInteger);


        //inputs amount is equal to random integer
        const ageInputs = HomePage.childrenInputsArray.length;
        assert.equal(ageInputs, randomInteger);   


        //ages are selected
        const areAgesSelected = HomePage.checkIfAgesAreSelected(); 
        assert(areAgesSelected);

        HomePage.clearChildrenValue();
    
    });

    it('User is provided with the same search form at search results page', () => {

        HomePage.open();

        //necessary for correct input values comparison
        browser.pause(3000)
        HomePage.chooseEnglishLanguage();


        //random city
        HomePage.chooseCity('Rivne')
        const cityHomePage = HomePage.cityValue;


       //randome date 
        HomePage.chooseRandomDate();
        const [HomePagefirstDateSeconds, HomePagelastDateSeconds ] = HomePage.getMiliseconds();
         

        //select random amount of people
        HomePage.selectPeopleInputButton.click();      
        HomePage.selectRandomPeopleAmount();
            const adultsHome = HomePage.adultsAmountValue;
            const childrenHome = HomePage.childrenAmountValue;
            const roomsHome = HomePage.roomsAmountValue;
        HomePage.selectChildrenAge();        
        
        if(HomePage.onMapCheckpoint.isDisplayed()) {
            HomePage.onMapCheckpoint.click(); 
        }

        //array of child ages
        const agesHome = HomePage.getChildAges();


        //submit
        HomePage.submit();


        // correct city
        const cityHotelsPage = Hotels.cityValue;
        const isCityCorrect = cityHomePage.includes( cityHotelsPage );
        assert( isCityCorrect );
      

        //correct date
        Hotels.calendar.click();
        const isDatesCorrect = 
                HomePagefirstDateSeconds == Hotels.firstDateSeconds && 
                HomePagelastDateSeconds == Hotels.lastDateSeconds;
        assert( isDatesCorrect );

 
       //correct adult amount 
        const adultsHotels = Hotels.adultsAmountValue;
        assert.equal( adultsHome, adultsHotels );

       
        //correct child amount  
        const childrenHotels = Hotels.childrenAmountValue;
        assert.equal( childrenHome, childrenHotels );


        //correct rooms amount   
        const roomsHotels = Hotels.roomsAmountValue;
        assert.equal( roomsHome, roomsHotels );

        
        //is ages correct
        const agesHotels = Hotels.getChildAges();  
        const areAgesCorrect = Helpers.checkArraysMatching( agesHome, agesHotels );     
        assert(areAgesCorrect);

    });

    it('Resulting search entries are taken based on filter', () => {

        HomePage.open();
        browser.pause(3000)
        HomePage.chooseEnglishLanguage();
        HomePage.chooseCity('Lviv'); 
         
        HomePage.selectPeopleInputButton.click(); 
        HomePage.clearChildrenValue();
        //check off map if exist
        if(HomePage.onMapCheckpoint.isDisplayed()) {
            HomePage.onMapCheckpoint.click(); 
        }  
        HomePage.submit();
       

        //comparison between chosen rating and rating of each hotel on the page
        const starsArrayOfResults = [];
        for( let i = 0; i < Hotels.checkboxesArray.length; i++ ) {
            Hotels.checkboxesArray[i].click();     
            browser.pause(5000);
            const stars = Hotels.checkboxesArray[i].getAttribute('data-value');
            if (stars === 'Unrated') {         
                Hotels.hotelsArray.forEach( hotel => {
                let unratedHotels = hotel.getAttribute('data-class');
                starsArrayOfResults.push(unratedHotels === '0' || unratedHotels === null);
                })             
            } else {
                Hotels.hotelsArray.forEach ( hotel => {
                    let hotelStars = hotel.getAttribute('data-class');
                    if( hotelStars === '0' ) {
                    let bookingStars = hotel.$$('span svg.-iconset-square_rating').length;         
                     hotelStars = bookingStars.toString();
                    }
                    starsArrayOfResults.push(hotelStars === stars || hotelStars === null);          
                })
            }      
            Hotels.checkboxesArray[i].click();
            browser.pause(1500);
        } 
        const isStarsAmountCorrect = !starsArrayOfResults.includes(false);  
        assert(isStarsAmountCorrect);



        //each hotel has rating corresponding to chosen
        const scoresArrayOfResults = [];
        for( let i = 0; i < Hotels.reviewScoresArray.length; i++) {
            Hotels.reviewScoresArray[i].click();
            browser.pause(3000);
            const scores = Hotels.reviewScoresArray[i].getAttribute('data-value');
            if (scores === '999') {
                Hotels.hotelsArray.forEach( hotel => {
                    const unscored = hotel.getAttribute('data-score');
                    scoresArrayOfResults.push( typeof unscored !== 'number' );
                })
            } else {
                Hotels.hotelsArray.forEach( hotel => {
                    const hotelScore = hotel.getAttribute('data-score');
                    const chosenScore = scores / 10;
                    scoresArrayOfResults.push( hotelScore >= chosenScore );
                }) 
            }
            Hotels.reviewScoresArray[i].click();
            browser.pause(1500);
        }
        const isRatingCorrect = !scoresArrayOfResults.includes(false); 
        assert(isRatingCorrect);
   
    });

    it('User is required to specify booking date to see booking price', () => {

        HomePage.open();    
        browser.pause(3000)
        HomePage.chooseCityFromList();

        //is page with hotels opened
        browser.pause(5000) 
        const areHotelsOpened = Hotels.hotelsContainer.isExisting()
        console.log("page opened: " + areHotelsOpened)
        assert(areHotelsOpened);

        
        // is calendar opened before click on Details button
        const isCalendarOpenedBefore = Hotels.checkIfCalendarOpened();
        console.log("calendar opened: " + isCalendarOpenedBefore)
        assert(isCalendarOpenedBefore);


        //are details visible before click on Details button
        const statusBefore = Hotels.checkHotelStatus();
        console.log("Hotel status: " + statusBefore)
        assert.equal(statusBefore, false);


        Hotels.clickRandomDetailsButton();

        
        //is calendar opened before click on Details button
        const isCalendarOpenedAfter = Hotels.checkIfCalendarOpened();
        assert(isCalendarOpenedAfter);

        //check random date in opened field
        browser.pause(1500)
        Hotels.chooseRandomDate();
        browser.pause(1500)
        Hotels.submit();


        //are details visible before click on Details button
        Hotels.hotelsContainer.waitForDisplayed(3000);
        browser.pause(3000);
        const statusAfter = Hotels.checkHotelStatus();
        assert.equal(statusAfter, true);
    
    });
});



