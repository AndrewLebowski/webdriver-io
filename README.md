# WebDriver IO 

Simple project using WebDriver IO on Booking.com


### Requirements

* Node: v10 and later
* npm:v6 and later


## Get started

- clone the project

### Install dependencies
- install your dependencies by running folowing command in terminal or command line:
 
```
npm install 
```
### Run Test

```
npm test
```

### Get report

```
npm run report
```


